( editor )
\ warnings off

( char manipulation )
: c@+        dup 1+ swap c@ ;
: c!+        swap over c! 1+ ;

( variables/constants )
30 constant row#
80 constant col#
variable buf 2400 allot

( rows and columns )
: ?validrow  row# < ;
: nrow#      dup ?validrow if exit then 30 - ;
: nrow       nrow# col# * ;
: ?validcol  80 < ;
: ncol       dup ?validcol if exit then 80 - ;
: nline      buf swap nrow + ;

( manipulation )
: call       >r ;
: rdrop      postpone r> postpone drop ; immediate
: ?exit      if rdrop exit then ;
: ?end       dup 1+ swap 29 = ;

( printing )
: .row       nline 79 type cr ;
: .bufloop   dup .row ?end ?exit recurse ;
: .buf       page  0 .bufloop drop ;

( clearing lines )
: ?count        swap 1+ dup 79 = ;
: clrchar       32 swap c!+ ;
: clrlineloop   clrchar ?count ?exit swap recurse ;
: clrline       nline 0 swap clrlineloop 2drop ;
: clrlinesloop  dup clrline ?end ?exit recurse ;
: clrlines      0 clrlinesloop drop ;

( shifting chars )
: ?eol         dup buf - 79 /mod drop 0= ;
: shiftn>     1+ dup c@ swap rot over c! ;
: shift>loop  shiftn> ?eol ?exit recurse ;
: shift>      dup c@ swap shift>loop 2drop ;
: shiftn<     dup 1+ c@ swap c!+ ;
: shift<loop  shiftn< ?eol ?exit recurse ;
: shift<      shift<loop 32 swap 1- c! ;

( writing )
: ?esc       dup 27 = ;
: ?<-        127 = ;
: <-         1- dup shift< swap drop ;
: i->        shift> c!+ ;
: insert1    over ?<- if <- exit then dup i-> ;
: overwrite  key ?esc ?exit swap c!+ .buf recurse ;
: insert     key ?esc ?exit swap insert1 .buf recurse ;

( search )
: c=  over swap c@+ rot = ;
: /setup  char 0 nline ;
: /loop   c= ?exit recurse ;
: /       /setup /loop 1- ;

( userland )
: gg     0 nline ;
: omode  overwrite .buf ;
: imode  insert .buf ;

: go  clrlines .buf gg omode quit ;
go
